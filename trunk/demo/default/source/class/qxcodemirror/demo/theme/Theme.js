/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("qxcodemirror.demo.theme.Theme",
{
  meta :
  {
    color : qxcodemirror.demo.theme.Color,
    decoration : qxcodemirror.demo.theme.Decoration,
    font : qxcodemirror.demo.theme.Font,
    icon : qx.theme.icon.Tango,
    appearance : qxcodemirror.demo.theme.Appearance
  }
});