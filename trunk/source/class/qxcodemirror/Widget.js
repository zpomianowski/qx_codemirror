/* ************************************************************************

   Copyright:
     2009 ACME Corporation -or- Your Name, http://www.example.com

   License:
     LGPL: http://www.gnu.org/licenses/lgpl.html
     EPL: http://www.eclipse.org/org/documents/epl-v10.php
     See the LICENSE file in the project's top-level directory for details.

   Authors:
     * Zbigniew Pomianowski

************************************************************************ */

/**
 * This is the main class of contribution "qxCodeMirror"
 *
 * TODO: Replace the sample code of a custom button with the actual code of
 * your contribution.
 *
 * @asset(qxcodemirror/*)
 */
qx.Class.define('qxcodemirror.Widget',
{
  extend: qx.ui.core.Widget,

  construct: function(resource_id, readOnly, getR, putR) {
    this.base(arguments);
    this.__resourceId = resource_id;
    this.__readOnly = readOnly || false;
    this.__get_route = getR || '/atms/manager/download/atms_attachments';
    this.__save_route = putR || '/atms/manager/file_api/atms_attachments';
    this.base(arguments);

    this.__addCss('qxcodemirror/codemirror.css');
    this.__addCss('qxcodemirror/monokai.css');
    var codeArr = [
        'qxcodemirror/codemirror-compressed.js'
    ];
    this.__loadScriptArr(codeArr, qx.lang.Function.bind(this.__loadElement, this));
    this.__setup();
  },

  statics: {
    INSTANCE_COUNTER: 0,
    LOADED: {},
    LOADING: {}
  },

  events: {
    scriptLoaded: 'qx.event.type.Event'
  },

  properties:
  {
    editor: {},

    loaded: {
      event: 'changeLoaded'
    },

    value: {
      event: 'changeValue',
      apply: '_applyValue'
    }
  },

  members: {
    __resourceId: null,
    __readOnly: null,

    __R: null,
    __S: null,

    __ws: null,

    __setup: function() {
      var that = this;
      this.__R = new qx.io.rest.Resource({
        get: {
          method: 'GET',
          url: that.__get_route + '/{id}'
        },
        put: {
          method: 'PUT',
          url: that.__save_route + '/{id}'
        }
      });
      this.__R.configureRequest(function(req) {
        if (!that.__readOnly)
          req.setCache(false);
      }, this);

      this.__R.addListener('putSuccess', function(e) {
        var response = e.getData();
        if (response == 'None') {
          jQuery('.flash').html(this.tr('File saved').toString());
          jQuery('.flash').show();
        } else {
          response = JSON.parse(response);
          var msg = response.content.caption + '\n\n' +
              this.tr('Errors') + ': ' +
              response.content.errors_count + '\n' +
              this.tr('Warnings') + ': ' +
              response.content.warnings_count + '\n';
          var data = response.content.data;
          for (var k in data) {
            msg += String(data[k].line + new Array(15).join(' ')).slice(0, 15);
            msg += String(data[k].incident + new Array(15).join(' ')).slice(0, 15);
            msg += data[k].desc + '\n';
          }

          var dialog = new atms.WinConfirmation(
            this.tr('Syntax errors!'),
            msg, null, null, null, {
              width: 800,
              height: 600,
              textStyle: 'textarea-code'
            });
          this.getApplicationRoot().add(dialog);
          dialog.open();
          jQuery('.flash').html(this.tr('Cannot save the file!').toString());
          jQuery('.flash').show();
        }
      }, this);

      this.__S = new qx.data.store.Rest(this.__R, 'get');
      this.__S.addListener('changeModel', function() {
        this.getEditor().doc.setValue(this.__S.getModel());
      }, this);
    },

    __loadScriptArr: function(codeArr, handler) {
      var that = this;
      var script = codeArr.shift();
      if (script) {
        if (qxcodemirror.Widget.LOADING[script]) {
          qxcodemirror.Widget.LOADING[script].addListenerOnce(
            'scriptLoaded', function() {
            this.__loadScriptArr(codeArr, handler);
          }, this);

        } else if (qxcodemirror.Widget.LOADED[script]) {
          this.__loadScriptArr(codeArr, handler);

        } else {
          qxcodemirror.Widget.LOADING[script] = this;
          var sl = new qx.bom.request.Script();
          var src = qx.util.ResourceManager.getInstance().toUri(script);
          sl.onload = function() {
            that.debug('Dynamically loaded ' + src + ': ' + status);
            that.__loadScriptArr(codeArr, handler);
            qxcodemirror.Widget.LOADED[script] = true;
            qxcodemirror.Widget.LOADING[script] = null;
            that.fireDataEvent('scriptLoaded', script);
          };

          sl.open('GET', src);
          sl.send();
        }
      } else {
        handler();
      }
    },

    __addCss: function(url) {
      if (!qxcodemirror.Widget.LOADED[url]) {
        qxcodemirror.Widget.LOADED[url] = true;
        var head = document.getElementsByTagName('head')[0];
        var el = document.createElement('link');
        el.type = 'text/css';
        el.rel = 'stylesheet';
        el.href = qx.util.ResourceManager.getInstance().toUri(url);
        setTimeout(function() {
          head.appendChild(el);
        }, 0);
      };
    },

    __loadElement: function() {
      var el = this.getContentElement().getDomElement();
      if (el == null) {
        this.addListenerOnce('appear', qx.lang.Function.bind(
          this.__loadElement, this), this);
      } else {
        qxcodemirror.Widget.INSTANCE_COUNTER++;
        var el = this.getContentElement().getDomElement();
        el.addEventListener('keydown', function(e) {
          if (e.ctrlKey && (e.which == 83)) {
            e.preventDefault();
            return false;
          }

          e.stopPropagation();
        });

        el.setAttribute('id', 'qxcodemirror_' + qxcodemirror.Widget.INSTANCE_COUNTER);
        el.setAttribute('overflow', 'scroll');

        // use loaded libraries
        var rulers = [], value = '';
        for (var i = 1; i <= 20; i++) {
          rulers.push({ column: i * 4 });
        }

        var that = this;
        var editor = CodeMirror(el, {
          theme: 'monokai',
          rulers: rulers,
          lineNumbers: true,
          indentUnit: 4,
          readOnly: this.__readOnly,
          styleActiveLine: true,
          extraKeys: {
            'Ctrl-S': function(cm) {
              that.saveFile();
            }
          }
        });
        this.setEditor(editor);
        this.getEditor().doc.setValue(
          this.tr('Please wait until it is loaded...'));
        this.fireEvent('loaded');

        if (this.__resourceId != null)
          this.__R.get({ id: this.__resourceId });

        this.addListener('destroy', this.__destroy);
      }
    },

    destroy: function() {
      if (this.__ws) this.__ws.close();
      arguments.callee.base.apply(this, arguments);
    },

    __destroy: function() {
      // destroy element
    },

    _applyValue: function(value, old) {
      var el = this.getContentElement().getDomElement();
      if (el == null) {
        this.addListenerOnce('loaded', qx.lang.Function.bind(
          this._applyValue, this, value, old), this);
      } else {
        this.getEditor().doc.setValue(value);
      }
    },

    syncFile: function() {
      this.__R.get({ id: this.__resourceId });
    },

    registerWsSource: function(channel) {
      var that = this;
      this.__ws = new atms.WebSocket(null, channel, function(e) {
        if (e.data)
            that.getEditor().doc.setValue(e.data);
      });
    },

    saveFile: function() {
      var that = this;
      var caption = this.tr('Save changes?');
      var desc = this.tr('This action cannot be undone.' +
          'Changes will be applied for all cases and templates tied with this attachemnt.');
      var dialog = new atms.WinConfirmation(
        caption,
        desc,
        function() {
          that.__R.put({ id: that.__resourceId },
              that.getEditor().getValue());
        });

      this.getApplicationRoot().add(dialog);
      dialog.open();
    }
  }
});
