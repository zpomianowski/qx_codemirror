/* ************************************************************************

   Copyright:
   2009 ACME Corporation -or- Your Name, http://www.example.com

   License:
   LGPL: http://www.gnu.org/licenses/lgpl.html
   EPL: http://www.eclipse.org/org/documents/epl-v10.php
   See the LICENSE file in the project's top-level directory for details.

   Authors:
   * Zbigniew Pomianowski

************************************************************************ */

/**
 * This is the main class of contribution 'qxCodeMirror'
 *
 * TODO: Replace the sample code of a custom button with the actual code of
 * your contribution.
 *
 * @asset(qxcodemirror/*)
 */
qx.Class.define('qxcodemirror.Editor',
{
  extend: qx.ui.container.Composite,

  construct: function(resource_id, readOnly, getR, putR) {
    this.base(arguments);
    this.__resourceId = resource_id;
    this.__readOnly = readOnly || false;
    this.base(arguments);
    this.setLayout(new qx.ui.layout.VBox());

    // Setup Toolbars
    var bar = new qx.ui.toolbar.ToolBar();
    var part1 = new qx.ui.toolbar.Part();
    var part2 = new qx.ui.toolbar.Part();

    var undoButton = new qx.ui.toolbar.Button(this.tr('Undo'), 'qxcodemirror/undo.png');
    var redoButton = new qx.ui.toolbar.Button(this.tr('Redo'), 'qxcodemirror/redo.png');
    var saveButton = new qx.ui.toolbar.Button(this.tr('Save'), 'qxcodemirror/save.png');
    saveButton.setEnabled(!readOnly);
    var refreshButton = new qx.ui.toolbar.Button(this.tr('Sync'), 'qxcodemirror/refresh.png');
    var helpButton = new qx.ui.toolbar.Button(this.tr('ATMS'), 'qxcodemirror/question.png');
    var appiumButton = new qx.ui.toolbar.Button(this.tr('Appium'), 'qxcodemirror/question.png');
    var pyAppiumButton = new qx.ui.toolbar.Button(this.tr('Appium-Python'), 'qxcodemirror/question.png');
    part1.add(undoButton);
    part1.add(redoButton);
    part1.add(saveButton);
    part1.add(refreshButton);
    part2.add(helpButton);
    part2.add(appiumButton);
    part2.add(pyAppiumButton);

    bar.add(part1);
    bar.add(part2);

    this.add(bar);
    this.setWidget(new qxcodemirror.Widget(resource_id, readOnly, getR, putR));
    this.add(this.getWidget(), { flex: 1 });

    undoButton.addListener('execute', function() {
      this.getWidget().getEditor().undo();
    }, this);

    redoButton.addListener('execute', function() {
      this.getWidget().getEditor().redo();
    }, this);

    saveButton.addListener('execute', function() {
      this.getWidget().saveFile();
    }, this);

    refreshButton.addListener('execute', function() {
      this.getWidget().syncFile();
    }, this);

    helpButton.addListener('execute', function() {
      qx.bom.Window.open('http://saut.test.misc.polsatc/saut_atms/default/wiki/scripting-testnode');
    });

    appiumButton.addListener('execute', function() {
      qx.bom.Window.open('http://appium.io/slate/en/master/?python#');
    });

    pyAppiumButton.addListener('execute', function() {
      qx.bom.Window.open('https://github.com/appium/python-client');
    });
  },

  properties: {
    widget: {}
  }
});
