/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("qxcodemirror.theme.classic.Theme",
{
  meta :
  {
    color : qxcodemirror.theme.classic.Color,
    decoration : qxcodemirror.theme.classic.Decoration,
    font : qxcodemirror.theme.classic.Font,
    appearance : qxcodemirror.theme.classic.Appearance,
    icon : qx.theme.icon.Oxygen
  }
});