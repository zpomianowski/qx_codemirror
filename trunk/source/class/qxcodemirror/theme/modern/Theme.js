/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("qxcodemirror.theme.modern.Theme",
{
  meta :
  {
    color : qxcodemirror.theme.modern.Color,
    decoration : qxcodemirror.theme.modern.Decoration,
    font : qxcodemirror.theme.modern.Font,
    appearance : qxcodemirror.theme.modern.Appearance,
    icon : qx.theme.icon.Tango
  }
});